# Université OpenClassrooms

**Cours :** Mettez en place l'intégration et la livraison continue.

**Dirigé par :** Professeur Laurent Grangeau.

**Exercice :** Mettez en place l'intégration et la livraison continues avec la démarche DevOps.

**URL du cours :** https://openclassrooms.com/fr/courses/2035736-mettez-en-place-lintegration-et-la-livraison-continues-avec-la-demarche-devops

**Travail présenté par :** Luciano Goulart [lgoulart@lnx-it.inf.br].

